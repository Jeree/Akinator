
public class Datos 
{
	 class Pais 
	{
	        public String nombre;
	        public boolean[] Respuestas;
	        
	        // Constructor de la clase
	       
	        public Pais(String nombre, boolean [] respuestas) 
	        	{
	            this.nombre = nombre;
	            this.Respuestas=respuestas;
	            }
	}
	
	Pais[] candidatos = new Pais[13];
	String[] preguntas = new String[12];
	
	public Datos () 
	{
		
		boolean[] arg= {false, true, false, false, false, false, true, true, false, true, true, false};
    	candidatos[0]= new Pais ("Argentina",arg);
    	boolean[] br ={true , true,  false, false, true,  true,  false, false, false, false, true, false}; 
    	candidatos[1] = new Pais ("Brasil", br);
    	boolean[] uy = {false, true,  false, false, false, false, true,  false,  true, false, false, false};
    	candidatos[2] = new Pais ("Uruguay", uy);
    	boolean[] py ={false, false, false, true,  true,  false, true,  false, false, false, false, false};
    	candidatos[3] = new Pais ("Paraguay", py);
    	boolean[] bl ={true,  false, false, true,  false, false, true,  false, false, true, true, false};
    	candidatos[4] = new Pais ("Bolivia",bl);          
    	boolean[] pe ={false, false, false,  true,  false, false, true,  false, false, true, true, true};
    	candidatos[5] = new Pais ("Peru", pe);
    	boolean[] vz ={false, true,  false, true,  true,  true,  true,  false, false, true, false, false};
    	candidatos[6] = new Pais ("Venezuela",vz);
    	boolean[] cl = {false, false, false,  false, true,  true,  true,  true,  false, true, false, true};
    	candidatos[7] = new Pais ("Chile",cl);      
    	boolean[] col = {false, true,  false,  false, true,  false, true,  false, false, true, true, true};
    	candidatos[8] = new Pais ("Colombia", col);
    	boolean[] su ={true,  true,  false, false, false, true,  false, false, false, false , false, false};
    	candidatos[9] = new Pais ("Surinam", su);
    	boolean[] ec = {false, false, false,  true,  true,  false, true,  false, true, false, false, true};
    	candidatos[10]= new Pais ("Ecuador",ec);
    	boolean[] gy ={true,  true,  false, false, false, false, false, false, false, true, false, false};
    	candidatos[11]= new Pais ("Guayana", gy);       
    	boolean[] gyf ={ false, true,  true, false, true,  false, false, false, true, true, false, false};
    	candidatos[12]= new Pais ("Guayana Francesa", gyf);
    	
    	preguntas[0]="�Su Bandera tiene color Verde?";
    	preguntas[1]="�El Mar Atlantico ba�a sus costas?";
    	preguntas[2]="�Es un colonia Francesa?";
    	preguntas[3]="�Su bandera tiene escudo?";
    	preguntas[4]="�Su bandera tiene color Azul(no Celeste)?";
    	preguntas[5]="�Su bandera tiene estrellas(no soles)?";
    	preguntas[6]="�Su idioma predominante es el Espa�ol?";
    	preguntas[7]="�Tiene regiones con nieve todo el A�o?";
    	preguntas[8]="�Solo limita con 2 paises?" ;
    	preguntas[9]="�Su nombre termina en vocal?" ;
    	preguntas[10]="�Esta entre los 5 mas grandes del continete Sud Americano?";
    	preguntas[11]="�El Mar Pacifico ba�a sus costas?";
	}
	
	public void quitarCandidatos( int indice )
	{
		candidatos[indice] = null;
	}
     public void quitarPregunta ( int indice )
     {
    	 preguntas[indice] = null;	 
     }
	
}