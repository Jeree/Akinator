import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;

@SuppressWarnings("serial")
public class InterfazAkinator extends JFrame 
{
	JPanel contentPane;
	boolean botonSi ;
	boolean botonNo;
	boolean botonNose;
	String textPregunta;
	JTextField pregunta;
	int contNose = 0 ;
	String respNose="";
	MetodosAkinator mA ;

	JButton btnNewButton ;
	JButton btnNewButton_1;
	JButton btnNewButton_2;
	
	
	public InterfazAkinator() 
	{
		mA = new MetodosAkinator () ;

		setTitle("AkinatorPaises");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		btnNewButton = new JButton("No");
		btnNewButton.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e)
			{
				botonSi= false;
				botonNo= true;
				botonNose= false;

				try 
				{

					mA.procesarPregunta(pregunta.getText() , botonSi , botonNo , botonNose );
				} 
				catch (Exception e1) 
				{
					e1.printStackTrace();
				}
				if ( mA.terminoJuego())
				{
					pregunta.setText(mA.mostrarAdivinado());
					ocultarBotones();
				}
				else
					pregunta.setText(mA.queSePregunta(mA.indicePregunta(mA.dts.preguntas, mA.cantidadDePreguntas(mA.dts.preguntas))));
				
			}
		}  
				);

		btnNewButton.setBounds(28, 117, 89, 23);
		contentPane.add(btnNewButton);

		 btnNewButton_1 = new JButton("No se");
		btnNewButton_1.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				botonSi= false;
				botonNo= false;
				botonNose= true;
				contNose++ ;

				if ( contNose >=  5 )
				{
					pregunta.setText("Apreto muchas veces nose. ");
					ocultarBotones();
				}
				else
				{
					try 
					{
						mA.procesarPregunta(pregunta.getText() , botonSi , botonNo , botonNose );
					} 
					catch (Exception e1) 
					{
						e1.printStackTrace();
					}

					if ( mA.terminoJuego())
					{	
							pregunta.setText(mA.mostrarAdivinado());
							ocultarBotones();
						
						
					}
					else
						pregunta.setText(mA.queSePregunta(mA.indicePregunta(mA.dts.preguntas, mA.cantidadDePreguntas(mA.dts.preguntas))));
				}
			}
		});		

		btnNewButton_1.setBounds(28, 152, 89, 23);
		contentPane.add(btnNewButton_1);

		btnNewButton_2 = new JButton("Si");
		btnNewButton_2.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e)
			{
				botonSi= true;
				botonNo= false;
				botonNose= false;
				
				try 
				{
					mA.procesarPregunta(pregunta.getText() , botonSi , botonNo , botonNose );
				} 
				catch (Exception e1) 
				{
					e1.printStackTrace();
				}
				if ( mA.terminoJuego())
				{
					pregunta.setText(mA.mostrarAdivinado());
					ocultarBotones();
				}
				else
					pregunta.setText(mA.queSePregunta(mA.indicePregunta(mA.dts.preguntas, mA.cantidadDePreguntas(mA.dts.preguntas))));
			}
		}
				);

		btnNewButton_2.setBounds(28, 83, 89, 23);
		contentPane.add(btnNewButton_2);

		JButton btnSalir = new JButton("Salir");
		btnSalir.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				System.exit(0);
			}
		}
				);

		btnSalir.setBounds(345, 228, 89, 23);
		contentPane.add(btnSalir);

		JLabel lblPregunta = new JLabel("");
		Image img= new ImageIcon(this.getClass().getResource("Imagenes/pensando.1.png")).getImage();
		lblPregunta.setIcon(new ImageIcon(img));
		lblPregunta.setBounds(227, 45, 177, 206);
		contentPane.add(lblPregunta);

	}

	public void textoDePantalla()
	{
		pregunta = new JTextField();
		pregunta.setEditable(false);
		pregunta.setText(mA.queSePregunta((mA.indicePregunta(mA.dts.preguntas, mA.cantidadDePreguntas(mA.dts.preguntas)))));
		pregunta.setBounds(42, 39, 350, 20);
		contentPane.add(pregunta);
		pregunta.setColumns(10);
	}
	
	public void ocultarBotones () 
	{
		btnNewButton.setVisible(false);
		btnNewButton_1.setVisible(false);
		btnNewButton_2.setVisible(false);
	}

	public static void main(String[] args) 
	{	
		VentanaInicio VentanainicioAkinator= new VentanaInicio();		
		VentanainicioAkinator.setVisible(true);
		VentanainicioAkinator.VentanaAkinator.textoDePantalla();		
	}
}
