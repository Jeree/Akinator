
public class MetodosAkinator 
{
	Datos dts ;

	public MetodosAkinator()
	{
		dts = new Datos () ;
	}

	public int indicePregunta ( String [] preguntas , int cantidadDePreguntas )
	{	
		int indicePregunta = 0 ; 
		for ( int indice = 0 ; indice < preguntas.length ;indice ++ )
		{
			if ( preguntas[indice] != null)
				indicePregunta = indice;
		}
		return indicePregunta ;
	}

	public int cantidadDePreguntas ( String [] preguntas )
	{
		int posiblesPreguntas = 0 ; 
		for ( int i = 0; i < preguntas.length; i++)
		{
			if ( preguntas[i] != null )
			{
				posiblesPreguntas++;
			}
		}
		System.out.println(posiblesPreguntas);
		return posiblesPreguntas ;
	}

	public void eliminarPreguntas ( int eliminada )
	{
		dts.quitarPregunta(eliminada);
	}

	public int numCandidatos ( Datos.Pais [] candidatos )
	{
		int numCandidatos = 0 ;
		for ( Datos.Pais i : candidatos )
		{
			if ( i != null )
				numCandidatos++;
		}
		return numCandidatos;
	}

	public String respuestaSelect ( boolean botSi , boolean botNo , boolean botNose )
	{
		if ( botSi == true && botNo == false && botNose == false )
			return "Si" ;
		else
			if ( botSi == false && botNo == true && botNose == false )
				return "No" ;
			else
				return "Nose" ;
	}

	public void compararRespuestas ( String respuesta , Datos.Pais [] candidatos, int indice )
	{
		boolean si = true ;
		boolean no = false ;

		if ( ! respuesta.equals("Nose")  )
		{
			for ( int i = 0 ; i < candidatos.length ; i++)
				if ( candidatos[i] != null)
					if ( respuesta.equals("Si"))
						if ( si != candidatos[i].Respuestas[indice]) 
							dts.quitarCandidatos(i); 
					else if ( respuesta.equals("No") )
						if ( no != candidatos[i].Respuestas[indice]) 
							dts.quitarCandidatos(i);
		}
	}

	public String queSePregunta ( int indice )
	{
		String preguntaNueva ;
		preguntaNueva = dts.preguntas[indice] ;
		return preguntaNueva ;
	}

	public int indiceActualDePregunta ( String preguntaActual )
	{
		int indiceDePregunta = 0 ;
		for( int i = 0 ; i < dts.preguntas.length ;i++)
		{
			if ( dts.preguntas[i] != null )
				if(dts.preguntas[i].equals(preguntaActual))
					indiceDePregunta = i;
		}
		return indiceDePregunta ;
	}

	public void procesarPregunta (String preguntaActual , boolean botonSi , boolean botonNo , boolean botonNose) 
	{
		int indicePreguntaActual = indiceActualDePregunta( preguntaActual ) ; 
		String respuesta = respuestaSelect ( botonSi , botonNo , botonNose );
		compararRespuestas(respuesta , dts.candidatos , indicePreguntaActual ) ;
		eliminarPreguntas ( indicePreguntaActual ) ;
	}

	public boolean terminoJuego ( )
	{
		if ( numCandidatos(dts.candidatos) <= 1 || cantidadDePreguntas(dts.preguntas) == 0)
			return true ;
		else
			return false;
	}

	public String mostrarAdivinado () 
	{
		if (cantidadDePreguntas(dts.preguntas)==0)
		{
				return ("Me quede sin preguntas");
		}		
		for ( int i = 0 ; i < dts.candidatos.length ; i++ )
		{
			
			if ( dts.candidatos[i] != null )
			return ("El pais que pienso es:  " + dts.candidatos[i].nombre);
		}
		return ("no adivine el pais");	
	}

}
