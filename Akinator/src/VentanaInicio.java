import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.Image;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import java.awt.Font;


@SuppressWarnings("serial")
public class VentanaInicio extends JFrame
{

	public JPanel contentPane;
	public boolean botonContinuar= false;
	InterfazAkinator VentanaAkinator= new InterfazAkinator();

	public VentanaInicio() 
	{
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JButton btnSalir = new JButton("Salir");
		btnSalir.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				System.exit(0);
			}
		});
		
		btnSalir.setBounds(335, 228, 89, 23);
		contentPane.add(btnSalir);

		JButton btnContinuar = new JButton("Continuar");
		btnContinuar.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
			{
				VentanaAkinator.setVisible(true);
				dispose();
			}
		});
		
		btnContinuar.setBounds(219, 191, 89, 23);
		contentPane.add(btnContinuar);
		
		JLabel lblNuevo = new JLabel();
		Image img= new ImageIcon(this.getClass().getResource("Imagenes/Pregunta2.1.png")).getImage();
		lblNuevo.setIcon(new ImageIcon(img));
		lblNuevo.setBounds(220, 11, 180, 169);
		contentPane.add(lblNuevo);
		
		JLabel lblPreguntas = new JLabel("");
		lblPreguntas.setFont(new Font("Tahoma", Font.PLAIN, 10));
		Image globoDePregunta= new ImageIcon(this.getClass().getResource("Imagenes/ParaPreguntar3.png")).getImage();
		lblPreguntas.setIcon(new ImageIcon(globoDePregunta));
		lblPreguntas.setBounds(102, 0, 188, 100);
		contentPane.add(lblPreguntas);
		
		JLabel lblBienvenido = new JLabel("Bienvenido");
		lblBienvenido.setBounds(147, 21, 77, 23);
		contentPane.add(lblBienvenido);
		
		JLabel lblTeAnimasA = new JLabel("Te animas a jugar?");
		lblTeAnimasA.setBounds(122, 37, 120, 14);
		contentPane.add(lblTeAnimasA);
		
		JLabel lblElijeUnPais = new JLabel("Elije un pais de America del Sur");
		lblElijeUnPais.setBounds(10, 88, 232, 14);
		contentPane.add(lblElijeUnPais);
		
		JLabel lblArgentinaBrasil = new JLabel("Argentina - Brasil - Uruguay - Paraguay");
		lblArgentinaBrasil.setBounds(10, 139, 232, 14);
		contentPane.add(lblArgentinaBrasil);
		
		JLabel lblBoliviaPeru = new JLabel("Bolivia - Peru - Colombia - Venezuela");
		lblBoliviaPeru.setBounds(10, 164, 232, 14);
		contentPane.add(lblBoliviaPeru);
		
		JLabel lblNewLabel = new JLabel("Chile - Surinam - Ecuador");
		lblNewLabel.setBounds(10, 189, 188, 14);
		contentPane.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Guyana - Guyana Francesa");
		lblNewLabel_1.setBounds(10, 211, 167, 14);
		contentPane.add(lblNewLabel_1);
	}
}
